# The program for fitting thermophysical data in approx. functions for numerical simulation

#import math
import numpy as np
import matplotlib.pyplot as plt

import cantera as ct


def CpCalc(T, Tm, R, high, low):
    if(T > Tm):
        a = high
        return R * (a[0] + T*(a[1] + T*(a[2] + T*(a[3] + T*a[4]))))
    else:
        a = low
        return R * (a[0] + T*(a[1] + T*(a[2] + T*(a[3] + T*a[4]))))


def lambdaCalc(T, Tm, high, low):
    if(T > Tm):
        a = high
        return 1e-4 * T**a[0] * np.exp(a[1]/T + a[2]/T**2 + a[3])
    else:
        a = low
        return 1e-4 * T**a[0] * np.exp(a[1]/T + a[2]/T**2 + a[3])


gas = ct.Solution('dryer2.cti')
p = ct.one_atm
T0 = 200.0
Tm = 1000.0
TN = 3000.0
R0 = 8314.0
W = 30.0
R = R0/W

Nx = 100
T = np.zeros(Nx)
lam = np.zeros(Nx)
Cp = np.zeros(Nx)

for i in range(0, Nx):
    T[i] = T0 + (TN - T0) * i/Nx
    gas.TPX = T[i], p, 'CH2O:1'
    Cp[i] = gas.cp_mass
    lam[i] = gas.thermal_conductivity
    
# lambda fit
# lambda(T) = 1e-4 * T^a0 * exp(a1/T + a2/T^2 + a3)
b = np.zeros(4)
A = np.zeros((4,4))

for i in range(0, Nx):
    A[0][0] += (np.log(T[i]))**2
    A[0][1] += np.log(T[i]) / T[i]
    A[0][2] += np.log(T[i]) / (T[i]**2)
    A[0][3] += np.log(T[i])
    b[0] += np.log(T[i]) * np.log(1e4*lam[i])

    A[1][0] += np.log(T[i]) / T[i]
    A[1][1] += 1.0 / (T[i]**2)
    A[1][2] += 1.0 / (T[i]**3)
    A[1][3] += 1.0 / T[i]
    b[1] += np.log(1e4*lam[i]) / T[i]
    
    A[2][0] += np.log(T[i]) / (T[i]**2)
    A[2][1] += 1.0 / (T[i]**3)
    A[2][2] += 1.0 / (T[i]**4)
    A[2][3] += 1.0 / (T[i]**2)
    b[2] += np.log(1e4*lam[i]) / (T[i]**2)

    A[3][0] += np.log(T[i])
    A[3][1] += 1.0 / T[i]
    A[3][2] += 1.0 / (T[i]**2)
    A[3][3] += 1.0 
    b[3] += np.log(1e4*lam[i])
        
lambda_coeffs = np.linalg.solve(A, b)

lam_fit = np.zeros(Nx)
for i in range(0, Nx):
    lam_fit[i] = lambdaCalc(T[i], Tm, lambda_coeffs, lambda_coeffs)


lambda_coeffs_Andrey = ( 3.72578566e-01, -1.22945428e+03, 1.68492466e+05, 5.21715368e+00 )

lam_fit_Andrey = np.zeros(Nx)
for i in range(0, Nx):
    lam_fit_Andrey[i] = lambdaCalc(T[i], Tm, lambda_coeffs_Andrey, lambda_coeffs_Andrey)

# Cp fit
# Cp(T) = R*(a0 + a1*T + a2*T^2 + a3*T^3 + a4*T^4)
b = np.zeros(5)
A = np.zeros((5,5))

for i in range(0, Nx):
    A[0][0] += 1.0
    A[0][1] += T[i]
    A[0][2] += T[i]**2
    A[0][3] += T[i]**3
    A[0][4] += T[i]**4
    b[0] += Cp[i]/R

    A[1][0] += 1.0 * T[i]
    A[1][1] += T[i] * T[i]
    A[1][2] += T[i]**2 * T[i]
    A[1][3] += T[i]**3 * T[i]
    A[1][4] += T[i]**4 * T[i]
    b[1] += Cp[i]/R * T[i]

    A[2][0] += 1.0 * T[i]**2
    A[2][1] += T[i] * T[i]**2
    A[2][2] += T[i]**2 * T[i]**2
    A[2][3] += T[i]**3 * T[i]**2
    A[2][4] += T[i]**4 * T[i]**2
    b[2] += Cp[i]/R * T[i]**2

    A[3][0] += 1.0 * T[i]**3
    A[3][1] += T[i] * T[i]**3
    A[3][2] += T[i]**2 * T[i]**3
    A[3][3] += T[i]**3 * T[i]**3
    A[3][4] += T[i]**4 * T[i]**3
    b[3] += Cp[i]/R * T[i]**3

    A[4][0] += 1.0 * T[i]**4
    A[4][1] += T[i] * T[i]**4
    A[4][2] += T[i]**2 * T[i]**4
    A[4][3] += T[i]**3 * T[i]**4
    A[4][4] += T[i]**4 * T[i]**4
    b[4] += Cp[i]/R * T[i]**4

Cp_coeffs = np.linalg.solve(A, b)

Cp_fit = np.zeros(Nx)
for i in range(0, Nx):
    Cp_fit[i] = CpCalc(T[i], Tm, R, Cp_coeffs, Cp_coeffs)

Cp_coeffs_Andrey = ( 1.66137566e+00, 9.25025266e-03, -4.45002979e-06, 1.03438991e-09, -9.49872206e-14 )

Cp_fit_Andrey = np.zeros(Nx)
for i in range(0, Nx):
    Cp_fit_Andrey[i] = CpCalc(T[i], Tm, R, Cp_coeffs_Andrey, Cp_coeffs_Andrey)

    
plt.plot(T, Cp_fit, label='fit')
plt.plot(T, Cp_fit_Andrey, label="Andrey's_fit")
plt.plot(T, Cp, label='input_data')
plt.legend()
plt.show()
